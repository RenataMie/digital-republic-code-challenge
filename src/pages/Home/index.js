import React from 'react';
import { useSelector } from 'react-redux';
import Card from '../../components/Card';

import { Title } from './style';

function Home() {
  const { wall, dimensions, wallAreas } = useSelector((state) => ({
    wall: state.wall.value,
    dimensions: state.dimensions,
    wallAreas: state.wallAreas,
  }));

  return (
    <>
      <Title> Paint your room!</Title>
      <Card wall={wall} dimensions={dimensions} wallAreas={wallAreas} />
    </>
  );
}

export default Home;
