import styled from 'styled-components';
import Brush from '../../public/images/brush2.png';

export const Title = styled.h1`
  font-size: 50px;
  font-weight: bold;
  color: white;
  text-align: center;
  margin-top: 50px;
  background: url(${Brush});
  background-repeat: no-repeat;
  background-size: 80% 80%;
  padding: 50px 0;
  background-position: center;
  font-family: 'Font', sans-serif;

  @media only screen and (min-width: 1024px) {
    background-size: 50% 80%;
  } ;
`;
