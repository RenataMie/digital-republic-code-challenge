import { createSlice } from '@reduxjs/toolkit';

export const counterSlice = createSlice({
  name: 'wall',
  initialState: {
    value: 1,
  },
  reducers: {
    increment: (state) => {
      state.value += 1;
    },
    reset: (state) => {
      state.value = 1;
    },
  },
});

export const { increment, reset } = counterSlice.actions;

export default counterSlice.reducer;
