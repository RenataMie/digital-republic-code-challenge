import { createSlice } from '@reduxjs/toolkit';

export const wallAreaSlice = createSlice({
  name: 'areas',
  initialState: {
    wall1: '',
    wall2: '',
    wall3: '',
    wall4: '',
  },
  reducers: {
    setAreas: (state, action) => {
      return { ...(state = action.payload) };
    },
  },
});

export const { setAreas } = wallAreaSlice.actions;

export default wallAreaSlice.reducer;
