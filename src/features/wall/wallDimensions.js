import { createSlice } from '@reduxjs/toolkit';

export const dimensionsSlice = createSlice({
  name: 'dimensions',
  initialState: {
    width: '',
    height: '',
    doors: '',
    windows: '',
  },
  reducers: {
    setDimensions: (state, action) => {
      return { ...(state = action.payload) };
    },
  },
});

// Action creators are generated for each case reducer function
export const { setDimensions } = dimensionsSlice.actions;

export default dimensionsSlice.reducer;
