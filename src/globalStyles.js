import { createGlobalStyle, css } from 'styled-components';
import MetrophobicWoff from './public/Font/metrophobic-v17-latin-regular.woff';
import MetrophobicWoff2 from './public/Font/metrophobic-v17-latin-regular.woff2';
import FontWoff from './public/Font/nothing-you-could-do-v13-latin-regular.woff';
import FontWoff2 from './public/Font/nothing-you-could-do-v13-latin-regular.woff2';

const fontFaces = css`
  @font-face {
    font-family: 'Metrophobic';
    src: url(${MetrophobicWoff2}) format('woff2'),
      url(${MetrophobicWoff}) format('woff');
  }
  @font-face {
    font-family: 'Font';
    src: url(${FontWoff2}) format('woff2'), url(${FontWoff}) format('woff');
  }
`;

const GlobalStyle = createGlobalStyle`
  ${fontFaces}
  * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
    background: white;
    font-size: 15px;
    color: pink;
    font-family: 'Metrophobic', sans-serif; 
  }

  .Toastify {

    .Toastify__toast {
      background:  #FC5300;

      .Toastify__toast-body {
        background: #FC5300 !important;
        margin-left: 25px;
  
        :before{
          content: 'ⓧ';
          font-weight: bold;
          font-size: 17px;
          top: 17px;
          left: 11px;
          position: absolute;
        }
      }

      .Toastify__toast-body > div:last-child {
        background: #FC5300 !important;
        font-size: 18px;
      }
  
      .Toastify__close-button svg{
        background: #FC5300 !important;
      }
    }
    
    @media only screen and (min-width: 1024px) {
      .Toastify__toast {
        background: #FC5300 !important;
      }
    }
}`;

export default GlobalStyle;
