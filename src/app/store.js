import { configureStore } from '@reduxjs/toolkit';
import dimensionsReducer from '../features/wall/wallDimensions';
import wallReducer from '../features/wall/wallNumber';
import wallAreas from '../features/wall/wallAreas';

export default configureStore({
  reducer: {
    wall: wallReducer,
    dimensions: dimensionsReducer,
    wallAreas,
  },
});
