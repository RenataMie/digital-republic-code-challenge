import styled from 'styled-components';
import Arrow from '../../public/images/arrow.png';

export const ButtonContainer = styled.button`
  width: auto;
  height: 60px;
  border: none;
  border-radius: 4px;
  position: relative;
  left: 60%;
  padding: 10px;
  cursor: pointer;
  color: #960018;
  background: transparent;
  background-image: url(${Arrow});
  background-repeat: no-repeat;
  background-size: 100% 100%;
  padding: 25px;
  background-position: center;
  font-weight: bold;

  &:hover {
    opacity: 0.5;
    animation: shake 0.82s cubic-bezier(0.36, 0.07, 0.19, 0.97);
    transform: translate3d(0, 0, 0);
    backface-visibility: hidden;
    perspective: 2000px;
  }

  @keyframes shake {
    10%,
    90% {
      transform: translate3d(-1px, 0, 0);
    }
    20%,
    80% {
      transform: translate3d(2px, 0, 0);
    }
    30%,
    50%,
    70% {
      transform: translate3d(-4px, 0, 0);
    }
    40%,
    60% {
      transform: translate3d(4px, 0, 0);
    }
  }

  @media only screen and (min-width: 768px) {
    left: 80%;
    bottom: 2%;
  }
`;
