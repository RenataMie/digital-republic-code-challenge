import React from 'react';
import { useDispatch } from 'react-redux';
import { ToastContainer, toast } from 'react-toastify';
import { ButtonContainer } from './style';
import { increment, reset } from '../../features/wall/wallNumber';
import { setAreas } from '../../features/wall/wallAreas';
import { calculateArea } from '../../utils';
import 'react-toastify/dist/ReactToastify.css';

function Button({ dimensions, wall, wallAreas }) {
  const dispatch = useDispatch();

  const handleCalculate = () => {
    const area = calculateArea(dimensions);
    const name = `wall${wall}`;

    if (area < 1) {
      toast('Too small! Wall area must be more than 1m².');
    } else if (area > 15) {
      toast('Too big! Wall area must be less than 15m².');
    } else if (area === 'error') {
      toast(
        'Too small! Doors and windows should not exceed more than half of the wall area!',
      );
    } else if (dimensions.doors && dimensions.height < 2.2) {
      toast('Too low! Walls that have doors must be at least 2.2m.');
    } else if (wall > 4) {
      dispatch(reset());
    } else {
      dispatch(
        setAreas({
          ...wallAreas,
          [name]: area,
        }),
      );
      dispatch(increment());
    }
  };

  return (
    <>
      <ButtonContainer onClick={handleCalculate}>
        {wall === 4 ? 'calculate total' : wall > 4 ? 'restart' : 'next wall'}
      </ButtonContainer>
      <ToastContainer hideProgressBar />
    </>
  );
}

export default Button;
