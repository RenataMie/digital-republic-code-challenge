import styled from 'styled-components';
import Brush from '../../public/images/brush2.png';

export const CardContainer = styled.div`
  background: ${(props) =>
    props.wall === 2 || props.wall === 4 ? 'white' : 'pink'};
  border: ${(props) =>
    props.wall === 2 || props.wall === 4 ? 'pink 5px solid' : 'none'};
  width: 90%;
  border-radius: 20px;
  margin: 50px auto;
  padding: 20px 0;

  @media only screen and (min-width: 1024px) {
    width: 50%;
  }
`;

export const CardTitle = styled.h1`
  color: #d52941;
  font-size: 30px;
  background: transparent;
  text-align: center;
  padding-top: 40px;
  font-family: 'Font', sans-serif;
`;

export const CardResult = styled.div`
  color: #d52941;
  font-size: 30px;
  background: transparent;
  text-align: center;
  padding: 40px 0;
`;

export const ResultInk = styled.p`
  color: white;
  width: 60%;
  margin: 0 auto;
  font-size: 30px;
  background: transparent;
  text-align: center;
  padding: 40px 0;
  background: url(${Brush});
  background-repeat: no-repeat;
  background-size: 100% 50%;
  background-position: center;
`;
