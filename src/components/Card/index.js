import React, { useEffect, useState } from 'react';
import Input from '../Input';
import Button from '../Button';
import { CardContainer, CardTitle, CardResult, ResultInk } from './style';
import { calculateInk } from '../../utils';

function Card({ wall, dimensions, wallAreas }) {
  const [inks, setInks] = useState({});

  useEffect(() => {
    setInks(calculateInk(wallAreas));
  }, [wallAreas]);

  return (
    <CardContainer wall={wall}>
      {wall > 4 ? (
        <>
          <CardTitle> FINAL RESULT: </CardTitle>
          <CardResult>
            You'll need
            <ResultInk>
              {inks.n18 !== 0 && `${inks.n18} can of 18L + `}
              {inks.n36 !== 0 && `${inks.n36} can of 3.6L + `}
              {inks.n25 !== 0 && `${inks.n25} can of 2.5L + `}
              {inks.n05 !== 0 && `${inks.n05} can of 0.5L `}
            </ResultInk>
            to paint your entire room!
          </CardResult>
          <Button dimensions={dimensions} wall={wall} />
        </>
      ) : (
        <>
          <CardTitle> Wall {wall} </CardTitle>
          <Input wall={wall} dimensions={dimensions} />
          <Button dimensions={dimensions} wall={wall} wallAreas={wallAreas} />
        </>
      )}
    </CardContainer>
  );
}

export default Card;
