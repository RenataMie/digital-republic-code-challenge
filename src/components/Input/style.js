import styled from 'styled-components';

export const InputContainer = styled.div`
  display: flex;
  background: transparent;
  justify-content: center;
  margin: 25px;
`;

export const InputArea = styled.input`
  background: transparent;
  border: none;
  outline: none;
  border-bottom: 1px solid black;
  margin: 20px 0 0 15px;
  width: 25%;
  height: 100%;
  color: black;
  font-size: 20px;
  -moz-appearance: textfield;
  appearance: textfield;

  &::-webkit-inner-spin-button {
    -webkit-appearance: none;
  }
`;

export const InputLabel = styled.h1`
  color: black;
  margin: 20px 0 0;
  background: transparent;
  fon-size: 20px;
`;
