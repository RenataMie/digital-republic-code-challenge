import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { InputArea, InputContainer, InputLabel } from './style';
import { setDimensions } from '../../features/wall/wallDimensions';

function Input({ wall, dimensions }) {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(
      setDimensions({
        width: '',
        height: '',
        doors: '',
        windows: '',
      }),
    );
  }, [wall, dispatch]);

  const handleInputChange = (event) => {
    const { value } = event.target;
    const { name } = event.target;

    dispatch(
      setDimensions({
        ...dimensions,
        [name]: value,
      }),
    );
  };

  return (
    <>
      {Object.entries(dimensions).map(([key, value]) => (
        <InputContainer key={key}>
          <InputLabel>
            {key === 'width' || key === 'height'
              ? `${key.charAt(0).toUpperCase() + key.slice(1)} (m):`
              : key === 'doors'
              ? ` How many ${key} ( default size 0.8m x 1.9m ):`
              : ` How many ${key} ( default size 2m x 1.2m ):`}
          </InputLabel>

          <InputArea
            type="number"
            name={key}
            value={value}
            onChange={(e) => handleInputChange(e)}
          />
        </InputContainer>
      ))}
    </>
  );
}

export default Input;
