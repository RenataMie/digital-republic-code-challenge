export const calculateArea = (dimensions) => {
  let area = +dimensions.width * +dimensions.height;

  if (dimensions.windows || dimensions.doors) {
    const totalDoorAndWindows =
      1.52 * +dimensions.doors + 2.4 * +dimensions.windows;

    if (totalDoorAndWindows > area / 2) return 'error';
    area -= totalDoorAndWindows;
  }

  return area;
};

export const calculateInk = (areas) => {
  let totalInk = {};

  const totalArea = Object.keys(areas).reduce(
    (sum, key) => sum + parseFloat(areas[key] || 0),
    0,
  );

  const litrosPerArea = totalArea / 5;

  const n18 = Math.floor(litrosPerArea / 18);
  const r18 = litrosPerArea % 18;
  const n36 = Math.floor(r18 / 3.6);
  const r36 = r18 % 3.6;
  const n25 = Math.floor(r36 / 2.5);
  const r25 = r36 % 2.5;
  const n05 = Math.ceil(r25 / 0.5);

  totalInk = { n18, n36, n25, n05 };

  return totalInk;
};
