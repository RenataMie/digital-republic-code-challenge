# Digital Republic Code Challenge

This project is part of Digital Republic recruitment process. A web application was made to help the user calculate the amount of paint needed to paint a room.

## Technologies

This project was created with:

- [React JS](https://reactjs.org/)
- [react-redux](https://react-redux.js.org/)
- [react-toastify](https://fkhadra.github.io/react-toastify/introduction)
- [styled-components](https://styled-components.com/)

## Installation

- To start the installation, clone the repository to your local folder or download:

` git clone https://gitlab.com/RenataMie/digital-republic-code-challenge.git`

- Install project dependencies:

`$ npm install`

- Run the app locally

`$ npm start`

- Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

## Deployment

- You can find a live demo here: [https://drchallenge.vercel.app/](https://drchallenge.vercel.app/)
